<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::get('/home', 'Admin\MainController@home');

Route::group(['prefix' => 'admin' , 'namespace' => 'Admin'], function () {
    Route::get('/report', 'MainController@report');
    Route::get('/client', 'MainController@client');
    Route::get('/add/report', 'MainController@addReport');
    Route::post('/add/report', 'MainController@CreateReport');

    Route::get('/report/delete/{id}', 'MainController@deleteReport');
    Route::get('/report/edit/{id}', 'MainController@editReport');
    Route::post('/report/edit/{id}', 'MainController@updateReport');

    Route::post('/report/telegram/{push_token}/{report_id}/{user_id}/{nameProject}', 'MainController@telegram');

    Route::get('/client/delete/{id}', 'MainController@deleteClient');
});

Auth::routes();
