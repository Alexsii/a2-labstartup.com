<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $guarded = ['id'];

    protected $casts = [
      'sending' => 'boolean'
    ];

    public function user()
    {
        return $this->belongsToMany('App\User', 'send_reports');
    }
}
