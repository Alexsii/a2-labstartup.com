<?php

namespace App\Listeners;

use App\Events\ChangesDone;
use App\Jobs\SendUpdateReport;
use App\Report;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendChangesReport
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChangesDone $event
     * @return void
     */
    public function handle(ChangesDone $event)
    {
        $old_report = Report::findOrFail($event->idReport());
        $new_report = $event->requestReport();
        $push_tokens = $old_report->user;


        if (($old_report['report'] != $new_report['report'])||($old_report['nameProject'] != $new_report['nameProject'])) {

            if ($push_tokens->toArray() == []) {
                $old_report->update($new_report->all());
                return redirect('/admin/report');
            }

            $old_report->update($new_report->all());

            foreach ($push_tokens as $push_token) {
                $push_token = $push_token->push_token;
                contains();
            }

            $notifyUpdateReport = 'There was an update in the project report "' . $new_report->nameProject . '"';

            dispatch(new SendUpdateReport($push_token, $notifyUpdateReport));
        }

    }
}
