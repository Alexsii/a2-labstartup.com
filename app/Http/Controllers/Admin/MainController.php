<?php

namespace App\Http\Controllers\Admin;

use App\Events\ChangesDone;
use App\Events\SomeEvent;
use App\Jobs\SendReportForUser;
use App\Report;
use App\SendReport;
use App\User;
use Davibennun\LaravelPushNotification\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use League\Flysystem\Exception;

class MainController extends Controller
{
    public function home()
    {
        return redirect('/admin/report');
    }

    public function report()
    {
        $reports = Report::all();
        $users = User::all();
        $count = $users->count();
        return view('admin.report.viewReports', compact('reports', 'users', 'count'));
    }

    public function client()
    {
        $clients = User::all();
        return view('admin.client.client', compact('clients'));
    }

    public function deleteClient($id)
    {
        User::findOrFail($id)->delete();
        return redirect('/admin/client');
    }

    public function addReport()
    {
        return view('admin.report.createReport');
    }

    public function CreateReport(Request $request)
    {
        event(new SomeEvent($request));
        return view('admin.report.createReport');
    }

    public function deleteReport($id)
    {
        Report::findOrFail($id)->delete();
        return redirect('/admin/report');
    }

    public function editReport($id)
    {
        $report = Report::findOrFail($id);
        return view('admin.report.updateReport', compact('report'));
    }

    public function updateReport($id, Request $request)
    {
        event(new ChangesDone($id, $request));
        return redirect('/admin/report');
    }

    public function telegram($push_token, $report_id, $user_id, $nameProject)
    {

        if (Report::findOrFail($report_id)->sending == false) {

            Report::findOrFail($report_id)->update(['sending' => true]);
            SendReport::create([
                'report_id' => $report_id,
                'user_id' => $user_id
            ]);

            $notifyReport = 'Attention! You have a new report in project "' . $nameProject . '"';

            $this->dispatch(new SendReportForUser($push_token, $notifyReport));
            return redirect('/admin/report');

        } else {

            return redirect('/admin/report');
        }
    }

}
