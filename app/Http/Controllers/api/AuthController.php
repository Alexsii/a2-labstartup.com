<?php

namespace App\Http\Controllers\api;


use App\Report;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use JWTAuth;

class AuthController extends Controller
{

    //================= SWAGGER
    /**
     * @SWG\Post(
     *     path="/api/v1/auth/login",
     *     summary="Check if database contains specified device token. If so - user authenticated, else register",
     *     tags={"auth"},
     *     description="Login",
     *     operationId="login",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="User email",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *         in="formData",
     *         description="User password",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="push_token",
     *         in="formData",
     *         description="Push token",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     )
     * )
     **/
    //================= SWAGGER

    /**
     * Check if specified device token exists in database
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    /*фв дейте*/
    public function login(Request $request)
    {

        $email = $request->email;
        if ($user = User::whereEmail($email)->first()) {
            if (password_verify($request['password'], $user['password'])) {
                $user->whereEmail($email)->update(['push_token' => $request->push_token ]);
                $token = JWTAuth::fromUser($user);

                return response()->json(compact('token'), 200);
            } else {
                return response()->json('Password error', 400);
            }
        } else {
            return response()->json('User email not found', 404);
        }
    }

    /**
     * @SWG\Post(
     *     path="/api/v1/auth/register",
     *     summary="Check if database contains specified device token. If so - user authenticated, else register",
     *     tags={"auth"},
     *     description="Register",
     *     operationId="Register",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="User email",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="User name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="last_name",
     *         in="formData",
     *         description="User name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *         in="formData",
     *         description="User password",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="push_token",
     *         in="formData",
     *         description="Push token",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     )
     * )
     **/


    public function registerUser(Request $request)
    {
        if (User::whereEmail($request->email)->first() == null) {
            $request['password'] = bcrypt($request['password']);
             User::create($request->all());
            return response()->json(true, 200);
        } else {
            return response()->json("User with such email already exists", 400);
        }
    }
}
