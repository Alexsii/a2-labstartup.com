<?php
/**
 * @SWG\Swagger(
 * basePath="",
 * host="a2labstartup.com",
 * schemes={"http"},
 * @SWG\Info(
 * version="1.0",
 * title="a2labstartup",
 * @SWG\Contact(name="A2-LAB", url="http://a2-lab.com/"),
 * ),
 * @SWG\Definition(
 * definition="Error",
 * required={"code", "message"},
 * @SWG\Property(
 * property="code",
 * type="integer",
 * format="int32"
 * ),
 * @SWG\Property(
 * property="message",
 * type="string"
 * )
 * )
 * )
 */