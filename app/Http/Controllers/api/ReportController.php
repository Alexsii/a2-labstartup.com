<?php

namespace App\Http\Controllers\api;

use App\SendReport;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use App;
use JWTAuth;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    //================= SWAGGER
    /**
     * @SWG\Get(
     *     path="/api/v1/report/show",
     *     summary="Check if database contains specified device token. If so - user authenticated, else register",
     *     tags={"report"},
     *     description="Login",
     *     operationId="login",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     )
     * )
     **/
    //================= SWAGGER

    /**
     * Check if specified device token exists in database
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */

    public function give_report()
    {
        $user = JWTAuth::parseToken()->authenticate();
        try {
            $reports_for_user = $user->reports;
        }catch (Exception $exception) {
            return response()->json(['error' => 'User not have report'], 404);
        }
            return response()->json(compact('reports_for_user'));
        }
}
