<?php

namespace App\Jobs;

use Davibennun\LaravelPushNotification\Facades\PushNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendUpdateReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $pushToken, $notifyUpdateReport;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($pushToken, $notifyUpdateReport)
    {
        $this->pushToken = $pushToken;
        $this->notifyUpdateReport = $notifyUpdateReport;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $pushToken = $this->pushToken;
        $notifyUpdateReport = $this->notifyUpdateReport;

        $massage = PushNotification::Message('Report update now', [
            'notifyUpdateReport' => json_encode($notifyUpdateReport)
        ]);
        PushNotification::app('AndroidApp')
            ->to($pushToken)
            ->send($massage);
    }
}
