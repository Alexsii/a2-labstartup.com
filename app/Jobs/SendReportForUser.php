<?php

namespace App\Jobs;

use Davibennun\LaravelPushNotification\Facades\PushNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendReportForUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $pushToken, $notifyReport;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($pushToken, $notifyReport)
    {
        $this->pushToken = $pushToken;
        $this->notifyReport = $notifyReport;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $pushToken = $this->pushToken;
        $notifyReport = $this->notifyReport;

        $massage = PushNotification::Message('New Report for you', [
            'notifyReport' => json_encode($notifyReport)
        ]);
        PushNotification::app('AndroidApp')
            ->to($pushToken)
            ->send($massage);
    }

    /**
     * @param Exception $exception
     */
}
