@extends('admin.index')
@section('styles')
    <link href="{{ url('/master/css/admin.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/report.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="{{ url('/master/css/home.css') }}" rel="stylesheet">
@endsection
@section('content')
    <main>
        <div class="form_box">
        {!! Form::model($report,[
        'method' => 'post',
        ]) !!}
        {!! Form::text('nameProject', null ,['class' => 'form-control', 'required' ,'maxlength' => '40' , 'style' => 'margin-bottom: 5px;']) !!}
        {!! Form::textarea('report', null, ['class' => 'form-control', 'required' ,'style' => 'margin-bottom: 5px; ']) !!}
        {!! Form::submit('Редактирывать отчёт', ['class' => 'btn btn-success', 'style' => 'float: right;']) !!}
        </div>
        {!! Form::close() !!}

    </main>

@section('scripts')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@endsection
@endsection