@extends('admin.index')
@section('styles')
    <link href="{{ url('/master/css/admin.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/report.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="{{ url('/master/css/home.css') }}" rel="stylesheet">
@endsection
@section('content')
    <main>
            {!! Form::model($reports,[
            'method' => 'get',
            ]) !!}

            <div class="form_box">
                <div>
                    <table class="table">
                        <thead style="width: 100%;">
                        <tr>
                            <th>Name project</th>
                            <th>Report</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        @foreach($reports as $report)
                            @if($report->sending == '0')
                            <tr style="width: 100%;">
                                <td style="width: 100px;"><p class="not_viewe">{{$report->nameProject}}</p></td>
                                <td style="width: 400px;"><p class="not_viewe">{{$report->report}}</p></td>
                                <td style="width: 60px;">
                                    <ul style="height: 27px; margin: 0 auto; display: block; width: 90px;">
                                        {{--href="{{url('/admin/report/telegram' .$report->id )}}"--}}
                                        <li class="options">
                                            <a id="telegram" onclick="send({{$report->id}})"
                                               class="fa fa-paper-plane fa-2x"
                                               style="text-decoration: none; cursor: pointer;">
                                            </a>
                                        </li>

                                        <li class="options">
                                            <a href="{{url('/admin/report/edit/' .$report->id )}}"
                                               class="fa fa-pencil-square-o fa-2x"
                                               style="text-decoration: none; cursor: pointer;">
                                            </a>
                                        </li>

                                        <li class="options">
                                            <a href="{{url('/admin/report/delete/' .$report->id )}}"
                                               class="fa fa-trash fa-2x"
                                               style="text-decoration: none ;cursor: pointer;">
                                            </a>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            @endif
                            <div id="form_send" class="form_sending">
                                <div class="send_box_close">
                                    <i id="close" onclick="close_send_form()"
                                       style="margin:-5px 0 0 1px; cursor: pointer;" class="fa fa-times-circle-o fa-2x"
                                       aria-hidden="true"></i>
                                </div>
                                <div class="send_box">
                                    @foreach($users as $user)
                                        {!! Form::open([
                                        'method' => 'post',
 'url' => '/admin/report/telegram/'. $user->push_token . '/' . $report->id . '/' . $user->id . '/' .$report->nameProject
                                        ]) !!}
                                        <div class="from_send">
                                            <h4>{{$user->name}}</h4>
                                            <button type="submit">Send</button>
                                        </div>
                                        {!! Form::close() !!}
                                    @endforeach
                                </div>
                            </div>
                        @endforeach


                            @foreach($reports as $report)
                                @if($report->sending == '1')
                                    <tr style="width: 100%;">
                                        <td style="width: 100px;; opacity: 0.7"><p class="not_viewe">{{$report->nameProject}}</p></td>
                                        <td style="width: 400px; opacity: 0.7"><p class="not_viewe">{{$report->report}}</p></td>
                                        <td style="width: 60px;">
                                            <ul style="height: 27px; margin: 0 auto; display: block; width: 60px;">
                                                <li class="options">
                                                    <a href="{{url('/admin/report/edit/' .$report->id )}}"
                                                       class="fa fa-pencil-square-o fa-2x"
                                                       style="text-decoration: none; cursor: pointer;">
                                                    </a>
                                                </li>
                                                <li class="options">
                                                    <a href="{{url('/admin/report/delete/' .$report->id )}}"
                                                       class="fa fa-trash fa-2x"
                                                       style="text-decoration: none ;cursor: pointer;">
                                                    </a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endif
                                <div id="form_send" data-id="{{ $report->id }}" class="form_sending">
                                    <div class="send_box_close">
                                        <i id="close" onclick="close_send_form()"
                                           style="margin:-5px 0 0 1px; cursor: pointer;" class="fa fa-times-circle-o fa-2x"
                                           aria-hidden="true"></i>
                                    </div>
                                    <div class="send_box">
                                        @foreach($users as $user)
                                            {!! Form::open([
                                            'method' => 'post',
     'url' => '/admin/report/telegram/'. $user->push_token . '/' . $report->id . '/' . $user->id . '/' .$report->nameProject
                                            ]) !!}
                                            <div class="from_send">
                                                <h4>{{$user->name}}</h4>
                                                <button type="submit">Send</button>
                                            </div>
                                            {!! Form::close() !!}
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                    </table>
                </div>
            </div>
            {!! Form::close() !!}
    </main>

@section('scripts')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
        function send(id) {
            $('.form_sending[data-id=' + id + ']').addClass('active_send');
        };
        function close_send_form() {
            $('.form_sending').removeClass('active_send');
        }
        ;

    </script>
@endsection
@endsection