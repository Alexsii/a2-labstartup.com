@extends('admin.index')
@section('styles')
    <link href="{{ url('/master/css/admin.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/report.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="{{ url('/master/css/home.css') }}" rel="stylesheet">
@endsection
@section('content')
    <main>
        {!! Form::model($clients,[
           'method' => 'get',
           ]) !!}

        <div class="form_box">
            <div>
                <table class="table">
                    <thead style="width: 100%;">
                    <tr>
                        <th>User name</th>
                        <th>User last name</th>
                        <th>E-mail</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                    @foreach($clients as $client)
                        <tr style="width: 100%;">
                            <td style="width: 100px;"><p class="not_viewe">{{$client->name}}</p></td>
                            <td style="width: 100px;"><p class="not_viewe">{{$client->last_name}}</p></td>
                            <td style="width: 300px;"><p class="not_viewe">{{$client->email}}</p></td>
                            <td style="width: 60px;">
                                <ul>
                                    <li class="options">
                                        <a href="{{url('/admin/client/delete/' .$client->id )}}"
                                           class="fa fa-trash fa-2x"
                                           style="text-decoration: none ;cursor: pointer;">
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        {!! Form::close() !!}
    </main>

@section('scripts')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@endsection
@endsection