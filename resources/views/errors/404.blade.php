<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .box_error {
            display: block;
            width: 100%;
            height: 100%;
        }

        .form_error {
            width: 500px;
            height: 250px;
            margin: 100px auto 0;
            color: #00bfa5;
            font-size: 38px;
            text-align: center;
            background: #263238;
            border-radius: 5px;
        }

        li {
            padding-top: 25px;
        }

        a {
            text-decoration: none;
            color: #00bfa5;
        }

        a:hover {
            color: #0f8676;
        }
    </style>
</head>
<body>
<div class="box_error">
    <div class="form_error">
        <ul style="list-style-type:none; padding-left: 0;">
            <li>Sory, web page not found</li>
            <li style="
                color: white;
                font-size: 42px;
                ">
                404
            </li>
            <li>
                <a href="/admin/report">
                    Go back to main page
                </a>
            </li>
        </ul>
    </div>
</div>
</body>
</html>